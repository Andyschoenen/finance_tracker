# frozen_string_literal: true

class TransactionsImporter::Default
  attr_reader :data, :account_id

  def initialize(data, account_id)
    @data = data
    @account_id = account_id
  end

  def self.call(data, account_id)
    self.new(data, account_id).call
  end

  def call
  end
end
