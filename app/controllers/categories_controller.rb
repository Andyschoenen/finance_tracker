class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /categories
  def index
    @categories = Category.eager_load(:transactions)
    if filtered?
      @categories = @categories.where(
                    'transactions.booking_date >= ? AND transactions.booking_date <= ?',
                    min_date_filter, max_date_filter
                  )
    end
  end

  # GET /categories/1
  def show
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # POST /categories
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name)
    end

    def min_date_filter
      return DateTime.new(params[:year].to_i, params[:month].to_i).beginning_of_month if params[:month].present?
      DateTime.new(params[:year].to_i).beginning_of_year
    end

    def max_date_filter
      return DateTime.new(params[:year].to_i, params[:month].to_i).end_of_month if params[:month].present?
      DateTime.new(params[:year].to_i).end_of_year
    end

    def filtered?
      return true if params[:year].present?
    end
end
