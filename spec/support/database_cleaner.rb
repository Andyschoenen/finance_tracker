# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.strategy = :truncation if run_without_database_cleaner?(example)

    DatabaseCleaner.cleaning do
      example.run
    end
  end

  def run_without_database_cleaner?(example)
    (example.metadata.keys & [:js, :no_database_cleaner]).any?
  end
end
