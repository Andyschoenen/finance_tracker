# frozen_string_literal: true

require 'rails_helper'

class BalanceableModel
  include Balanceable

  def transactions
    Transaction.all
  end

  def create_transaction(attributes)
    Transaction.create!(attributes)
  end
end

describe BalanceableModel do
  describe '#balance' do
    subject { model.balance }
    let(:model) { described_class.new }
    it { is_expected.to eq(0) }

    context 'with transactions' do
      let!(:transaction_1) { create(:transaction, value: 20_00) }
      let!(:transaction_2) { create(:transaction, value: 30_00) }

      it { is_expected.to eq(50_00) }
    end
  end
end
