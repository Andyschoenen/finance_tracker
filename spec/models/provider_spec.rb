# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Provider do
  describe '#transactions_importer' do
    subject { described_class.new(slug).transactions_importer }
    let(:slug) { 'sparda_bank' }

    it { is_expected.to eq(TransactionsImporter::SpardaBank) }

    context 'unknown slug' do
      let(:slug) { 'foo' }

      it { is_expected.to eq(TransactionsImporter::Default) }
    end
  end

  describe '#name' do
    subject { described_class.new(slug).name }
    let(:slug) { 'sparda_bank' }

    it { is_expected.to eq('Sparda Bank') }

    context 'unknown slug' do
      let(:slug) { 'foo' }

      it { is_expected.to eq('Default') }
    end
  end

  describe '.all' do
    subject { described_class.all }

    specify { expect(subject.count).to eq(1) }
    specify { expect(subject.first.slug).to eql(Provider.new('sparda_bank').slug) }
  end
end
